Install scripts for Arch Linux.

Steps:
1. Boot from live Arch disk (ie. USB or ISO)
2. Copy directory containing install scripts.
3. Configure variables in config.sh.
4. Run scripts in order.
