#!/bin/bash
LANGUAGE="en_ZA.UTF-8"

PARTITIONING_TOOL="cfdisk"
PARTITION_LISTING_COMMAND="fdisk -l"
# Use different partitioning tools for disks over 2TB
#PARTITIONING_TOOL="cgdisk"
#PARTITION_LISTING_COMMAND="gdisk -l"
TARGET_DEVICE="/dev/sda"
MAKE_FILE_SYSTEM_COMMAND="mkfs.ext4"

TIMEZONE="Africa/Johannesburg"
HOSTNAME="Arch"
USER="john"
GROUP="john"
ADDITIONAL_GROUPS="wheel"
USER_HOME="/home/john"
EXTRA_PACKAGES="bash-completion vim networkmanager rhythmbox xfce4-datetime-plugin xfce4-notifyd xfce4-screenshooter ristretto firefox thunderbird xfburn vlc xscreensaver gnome-system-monitor evince xfce4-pulseaudio-plugin wget rsync ntp galculator"
