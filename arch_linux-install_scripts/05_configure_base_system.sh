#!/bin/bash

# Generate fstab
echo "Generating fstab..."
genfstab -U -p /mnt >> /mnt/etc/fstab
echo "Confirm fstab is correct..."
read -p "Press ENTER to continue..."
vi /mnt/etc/fstab

# Copy scripts to installation
echo "Copying install scripts to /johncoffee-scripts/ on installed system."
mkdir /mnt/johncoffee-scripts/
cp -vir ./* /mnt/johncoffee-scripts/

# Chroot to installed system
echo "Chrooting to installed system..."
echo "Continue with 05_A_configure_base_system_part_2.sh in /johncoffee-scripts/"
arch-chroot /mnt /bin/bash
