#!/bin/bash

# Import configuration
source config.sh

# Install vim, sudo
pacman -S ${EXTRA_PACKAGES}

echo "Sudo needs to be configured."
read -p "Press ENTER to continue..."
visudo

echo "Setting user profile to keep environment when using sudo."
echo "# Preserve environment when running sudo" >> ${USER_HOME}/.bashrc
echo "alias sudo=\"sudo -E\"" >> ${USER_HOME}/.bashrc
