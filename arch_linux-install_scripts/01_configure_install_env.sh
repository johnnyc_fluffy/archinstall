#!/bin/bash

# Import configuration
source config.sh

# Set console font
setfont Lat2-Terminus16

# Set locale
echo "Set locale in /etc/locale.gen"
read -p "Press ENTER to continue..."
vi /etc/locale.gen
locale-gen
export LANG=${LANGUAGE}

# Test for internet connection
echo "Now testing for internet connection."
ping -c 4 archlinux.org
if [ $? -eq 0 ]; then
	echo "Continue with the 02_partition_install_target_location.sh script."
else
	echo "No Internet connection found. Aborting."
fi
