#!/bin/bash

# Import configuration
source config.sh

# Set locale
echo "Set locale in /etc/locale.gen"
read -p "Press ENTER to continue..."
vi /etc/locale.gen
locale-gen
echo LANG=${LANGUAGE} > /etc/locale.conf
export LANG=${LANGUAGE}

# Set console font
setfont Lat2-Terminus16
echo "FONT=Lat2-Terminus16" > /etc/vconsole.conf

# Set time zone
echo "Setting time zone to ${TIMEZONE}"
rm -rf /etc/localtime
ln -s /usr/share/zoneinfo/${TIMEZONE} /etc/localtime

# Set the hostname
echo "Setting hostname."
echo ${HOSTNAME} > /etc/hostname

# Configure pacman
echo "Please review /etc/pacman.conf now."
read -p "Press ENTER to continue..."
vi /etc/pacman.conf
pacman -Sy

# Set root password
echo "Set the root password."
passwd

# Create user
echo "Creating a new user account for normal operations."
groupadd ${GROUP}
useradd -m -g ${GROUP} -G ${ADDITIONAL_GROUPS} -s /bin/bash ${USER}
echo "Set password for new user account."
passwd ${USER}
