#!/bin/bash

# Import configuration
source config.sh

pacman -S xfce4 ttf-dejavu gvfs network-manager-applet pulseaudio pulseaudio-alsa pavucontrol

echo "#!/bin/sh" >> ${USER_HOME}/.xinitrc
echo "#" >> ${USER_HOME}/.xinitrc
echo "# ~/.xinitrc" >> ${USER_HOME}/.xinitrc
echo "#" >> ${USER_HOME}/.xinitrc
echo "# Executed by startx (run your window manager from here)" >> ${USER_HOME}/.xinitrc
echo "" >> ${USER_HOME}/.xinitrc
echo "if [ -d /etc/X11/xinit/xinitrc.d ]; then" >> ${USER_HOME}/.xinitrc
echo "  for f in /etc/X11/xinit/xinitrc.d/*; do" >> ${USER_HOME}/.xinitrc
echo "    [ -x \"\$f\" ] && . \"\$f\"" >> ${USER_HOME}/.xinitrc
echo "  done" >> ${USER_HOME}/.xinitrc
echo "  unset f" >> ${USER_HOME}/.xinitrc
echo "fi" >> ${USER_HOME}/.xinitrc
echo "" >> ${USER_HOME}/.xinitrc
echo "# exec gnome-session" >> ${USER_HOME}/.xinitrc
echo "# exec startkde" >> ${USER_HOME}/.xinitrc
echo "# exec startxfce4" >> ${USER_HOME}/.xinitrc
echo "# ...or the Window Manager of your choice" >> ${USER_HOME}/.xinitrc
echo "exec startxfce4" >> ${USER_HOME}/.xinitrc

chown ${USER}:${GROUP} ${USER_HOME}/.xinitrc
cp -vir /etc/skel/.bash_profile ${USER_HOME}/
echo "[[ -z \$DISPLAY && \$XDG_VTNR -eq 1 ]] && exec startx" >> ${USER_HOME}/.bash_profile
chown ${USER}:${GROUP} ${USER_HOME}/.bash_profile
