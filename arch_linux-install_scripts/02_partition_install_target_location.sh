#!/bin/bash

# Import configuration
source config.sh

echo "Partition the target location now."
echo "Recommended partitions:"
echo "  2G partition for swap space"
echo "  Remaining amount for installation."
read -p "Press ENTER to continue..."
${PARTITIONING_TOOL} ${TARGET_DEVICE}

${PARTITION_LISTING_COMMAND} ${TARGET_DEVICE}
read -p "Enter the full partition name to install to: " MAIN_PARTITION
read -p "Enter the full partition name for swap space: " SWAP_PARTITION
${MAKE_FILE_SYSTEM_COMMAND} ${MAIN_PARTITION}
mkswap ${SWAP_PARTITION}
swapon ${SWAP_PARTITION}

echo "Mounting new installation."
mount ${MAIN_PARTITION} /mnt/
