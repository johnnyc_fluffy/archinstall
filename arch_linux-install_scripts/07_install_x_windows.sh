#!/bin/bash

echo "Checking for Internet connection..."
ping -c 4 archlinux.org

if [ $? -ne 0 ]; then
	echo "No Internet connection found. Attempting to enable dhcpcd..."
	systemctl start dhcpcd && sleep 10 &&
	ping -c 4 archlinux.org
	if [ $? -ne 0 ]; then
		echo "No Internet connection found. Aborting."
		exit
	fi
fi
pacman -S xorg-server xorg-xinit mesa
echo "A video driver may need to be installed as well. Please install the correct one for your video card before continuing."
echo "A list of video drivers available can be found by running:"
echo "pacman -Ss xf86-video"
