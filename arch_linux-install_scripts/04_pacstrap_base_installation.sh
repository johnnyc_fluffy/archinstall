#!/bin/bash

echo "Ready to install base system. Select the base packages to install."
echo "Installing all packages is recommended."
pacstrap -i /mnt base base-devel
