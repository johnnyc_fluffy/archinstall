#!/bin/bash

# Import configuration
source config.sh

# Install GRUB
pacman -S grub os-prober
grub-install --target=i386-pc --recheck ${TARGET_DEVICE}
cp -vir /usr/share/locale/en\@quot/LC_MESSAGES/grub.mo /boot/grub/locale/en.mo
grub-mkconfig -o /boot/grub/grub.cfg
echo "GRUB installed."
echo "The system needs to be rebooted now."
echo "Run the following commands:"
echo "  exit"
echo "  umount /mnt"
echo "  reboot"
